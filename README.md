# Find Github Users
### Problem statement
Using the GitHub API, design a service that would enable for a user to search for an
 existing GitHub username, and upon success, returns the following information:
* The searched user's GitHub handle(linkable back to user's GitHub profile page).
* Searched user's follower count.
* A list of the searched user's followers(user avatar, username)
* Searched user followed count and number of public repositories the user contributed to.

A "load more" button will be implemented to return additional user followers
as GitHub API is designed to return a maximum of 30 results per request. For this application, it was 
configured to return 20 results per request.

### Solution
The [Angular 2](https://angular.io/) JavaScript Framework was selected for this project. This framework is designed for 
single page applications(a requirement of this project). This framework is written using a superset of JavaScript called TypeScript and 
 simulates multi-page web application behaviour using components (which are just Directives with a template), and swapping these components 
 to retrieve the requested view. This framework supports end to end testing with protractor and 
unit test with Jasmine to enable sufficient test coverage. 
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.3.
This generated boilerplate code for the index.html page and app component.
In addition to the cli generated code, one service and four components were generated including:
* **search-user.component** : This component instantiates the model which the search value is assigned to, and then performs validation
on the search object before passing it to the returned-user.component.
* **returned-user.component** : This component gets the service object through dependency injection
and returns the user object asynchronously as an Observable object. This is to ensure
  the app is still responsive even if an error occurs while fetching the data.
* **github-user.service** has two method which returns a user object and a list of followers for
a given user respectively as Observable. This service uses dependency injection
to inject an instance of the Http object which uses it's get protocol to retrieve the results


An interface is used as a contract binder for the User object class to
ensure all the necessary object components are implemented.

An app-routes class is used to define the the application routes.

Sassy CSS or SCSS was used for styling the application as this is a more
maintainable way to style web applications. Also, it saves a lot of typing with
defining common variables.

Jasmine and Karma was used to create unit tests to test basic functionality of the application.

For the "load more" functionality, a variable was used to keep track of the 
total followers returned by service.

With more time, I would write more unit tests to provide better test coverage and create 
a more stylish user interface.

The url for the working site which was deployed to heroku is
[GitHub user finder](https://aqueous-dusk-80060.herokuapp.com/).
I took advantage of the deployment strategy offered on this [Site](https://medium.com/@ryanchenkie_40935/angular-cli-deployment-host-your-angular-2-app-on-heroku-3f266f13f352) to enable me deploy my app to Heroku. 
 
One of the major drawbacks of the aot(Ahead of Time) compilation for this app in production mode is that
all models or objects that need to be represented in a view have to be declared public in order to compile.
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
