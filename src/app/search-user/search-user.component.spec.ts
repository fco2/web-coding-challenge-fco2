import {ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchUserComponent } from './search-user.component';
import { FormsModule } from '@angular/forms';
import {Router} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {RouterTestingModule} from "@angular/router/testing";

describe('SearchUserComponent', () => {
  let component: SearchUserComponent;
  let fixture: ComponentFixture<SearchUserComponent>;


  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchUserComponent ],
      imports: [HttpModule, BrowserModule, FormsModule],
      providers: [{provide: Router, useValue: MouseEvent}, RouterTestingModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
