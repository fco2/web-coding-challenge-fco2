import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-search-user',
  templateUrl: './search-user.component.html',
  styleUrls: ['search-user.component.scss']
})
export class SearchUserComponent implements OnInit {

  Username: string = '';
  modelFailsValidation: boolean;

  constructor(private router: Router) {
    this.modelFailsValidation = false;
  }

  ngOnInit() {
  }

  searchUser(){
    this.goToResultsUrl();
  }

  goToResultsUrl(){
    if(this.Username.trim() === ''){
      this.modelFailsValidation = true;
      return;
    }
    this.router.navigate(['/returned-user/', this.Username]);
  }

}
