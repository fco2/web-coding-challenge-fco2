import {ComponentFixture, TestBed, inject} from '@angular/core/testing';

import { ReturnedUserComponent } from './returned-user.component';
import { RouterTestingModule } from '@angular/router/testing';
import {GithubUserService} from "../services/github-user.service";
import {Http, BaseRequestOptions} from "@angular/http";
import {MockBackend} from "@angular/http/testing";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";


describe('ReturnedUserComponent', () => {
  let component: ReturnedUserComponent;
  let fixture: ComponentFixture<ReturnedUserComponent>;

  // variables
  let mockBackend: MockBackend;
  let activatedRoute: ActivatedRoute;
  let location: Location;
  let githubService: GithubUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnedUserComponent ],
      imports: [RouterTestingModule],
      providers: [
        {
          provide: Http,
          useFactory: (backend: MockBackend, options: BaseRequestOptions) => new Http(backend, options),
          deps: [ MockBackend, BaseRequestOptions ]
        }, GithubUserService, MockBackend,
        BaseRequestOptions
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReturnedUserComponent);
  });

  beforeEach(inject([GithubUserService, ActivatedRoute, Location, MockBackend],
    (gus: GithubUserService, ar: ActivatedRoute, loc: Location, mbd: MockBackend) => {
        githubService = gus;
        activatedRoute = ar;
        location = loc;
        mockBackend = mbd;
    }));

 it('should be created', () => {
   component = fixture.componentInstance;
   fixture.detectChanges();
   expect(component).toBeDefined();
  });
});
