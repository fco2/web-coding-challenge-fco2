import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {GithubUserService} from "../services/github-user.service";
import {User} from "../models/user";
import {Location} from "@angular/common";

@Component({
  selector: 'app-returned-user',
  templateUrl: './returned-user.component.html',
  styleUrls: ['./returned-user.component.scss']
})
export class ReturnedUserComponent implements OnInit, OnDestroy {
  Username: string = '';
  UserData: User = new User();
  Followers: User[] = [];
  private sub: any;
  private followerSub: any;
  private pageCount: number = 1;
  isFollowerLimitReached: number;
  private errorValue: any;


  constructor(private activatedRoute: ActivatedRoute,
              private githubService: GithubUserService,
              private location: Location) {
    this.isFollowerLimitReached = 0;
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.Username = params['username'];
    });
    this.getUser();
    this.getFollowers();
  }

  getUser(){
    this.sub = this.githubService.getUser(this.Username)
      .subscribe(
        res => this.setUserData(res)
      );
  }

  setUserData(res){
    this.UserData = res;
  }

  getFollowers(){
    this.followerSub = this.githubService.getUserFollowers(this.Username)
     .subscribe(res => this.setFollowersData(res),
       err => this.errorValue = err);
  }

  setFollowersData(res){
    this.Followers = res;
    this.setIsFollowerLimitReached();
  }

  goBack(){
    this.location.back();
  }

  loadMoreUsers(){
    this.pageCount += 1;

    this.followerSub = this.githubService.getUserFollowers(this.Username, this.pageCount)
      .subscribe(res => this.addNextBatchOfFollowers(res),
                err => this.errorValue = err);
  }

  addNextBatchOfFollowers(res){
    this.Followers = this.Followers.concat(res);
    this.setIsFollowerLimitReached();
  }

  setIsFollowerLimitReached(): void {
    this.isFollowerLimitReached = (this.Followers.length - Number.parseInt(this.UserData.followers));
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.followerSub.unsubscribe();
  }
}
