import {TestBed} from '@angular/core/testing';

import { AppComponent } from './app.component';
import {AppModule} from "./app.module";
import { RouterTestingModule } from '@angular/router/testing';
import {RouterLinkStubDirective, RouterOutletStubComponent} from "./test-stub-helpers/router-stubs";

describe('AppComponent', () => {
  let component: AppComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [AppModule, RouterTestingModule]
    });
    const fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should have defined component', () => {
    expect(component).toBeDefined();
  });
});
