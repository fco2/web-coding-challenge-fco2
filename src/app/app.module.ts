import { BrowserModule } from '@angular/platform-browser';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import { AppComponent } from './app.component';
import { SearchUserComponent } from './search-user/search-user.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import {AppRoutes} from "./app-routes";
import { ReturnedUserComponent } from './returned-user/returned-user.component';
import {GithubUserService} from "./services/github-user.service";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import { APP_BASE_HREF } from '@angular/common';
import { AboutComponent } from './about/about.component';
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    AppComponent,
    SearchUserComponent,
    AppHeaderComponent,
    ReturnedUserComponent,
    AboutComponent
  ],
  imports: [
    RouterModule,
    AppRoutes,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [GithubUserService, { provide: APP_BASE_HREF, useValue: '/' }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
