import {IUser} from "../interfaces/iuser";

export class User implements IUser{
  login: string;
  public_repos: string;
  avatar_url: string = "";
  followers: string;
  following: string;
  html_url: string;
  constructor(){}
}
