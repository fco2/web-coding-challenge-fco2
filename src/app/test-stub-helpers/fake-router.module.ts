import {RouterOutletStubComponent, RouterLinkStubDirective} from "./router-stubs";
import {AppModule} from "../app.module";
import {NgModule} from "@angular/core";
/**
 * Created by Frank on 6/26/2017.
 */
@NgModule({
  imports: [
    AppModule
  ],
  declarations: [
    RouterLinkStubDirective,
    RouterOutletStubComponent
  ]
})
export class FakeRouterModule {
}
