import { TestBed, inject } from '@angular/core/testing';

import { GithubUserService } from './github-user.service';
import {Http, BaseRequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import {MockBackend} from "@angular/http/testing";

@Injectable()
class MockedService {
  constructor(private _http: Http) {}
}

describe('GithubUserService', () => {

  let testService = {
    githubBasePath: '',
    http: {_backend: '', _defaultOptions: ''},
    getUser(username) {
      return this._http.get(username).map(res => res.json());
    },
    getUserFollowers(username, count){
      return this._http.get(username).map(res => res.json());
    }
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: Http,
        useFactory: (backend, options) => {return new Http(backend, options);},
        deps: [MockBackend, BaseRequestOptions]
      },
      MockBackend, BaseRequestOptions, MockedService, {provide: GithubUserService, useValue: testService}],
      imports: []
    });
  });

  it('should be created', inject([GithubUserService], (service: GithubUserService) => {
    expect(service).toBeTruthy();
  }));
});
