import { Injectable } from '@angular/core';
import {Http, Headers, Response} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs";
import {User} from "../models/user";

@Injectable()
export class GithubUserService {

  private githubBasePath: string = 'https://api.github.com/users';

  constructor(private http: Http) { }

  getUser(username: string): Observable<User>{
    return this.http.get(`${this.githubBasePath}/${username}`, {headers: GithubUserService.getHeader()})
      .map(response => response.json())
      .catch(this.HandleErrors);
  }

  getUserFollowers(username: string, page_number: number = 1): Observable<User[]>{
    return this.http.get(`${this.githubBasePath}/${username}/followers?page=${page_number}&per_page=20`,
      {headers: GithubUserService.getHeader()})
      .map(response => response.json())
      .catch(this.HandleErrors);
  }

  private static getHeader(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return headers;
  }

  private HandleErrors(error: Response): any{
    return Observable.throw(error.json().error() || 'A server error occurred!')
  }
}
