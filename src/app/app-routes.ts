import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {SearchUserComponent} from "./search-user/search-user.component";
import {ReturnedUserComponent} from "./returned-user/returned-user.component";
import {AboutComponent} from "./about/about.component";

const routes: Routes = [
  {path: 'search', component: SearchUserComponent},
  {path: 'returned-user/:username', component: ReturnedUserComponent},
  {path: 'about', component: AboutComponent},
  {path: '', redirectTo: '/search', pathMatch: 'full'}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})

export class AppRoutes {
}
