import { FindGithubUsersPage } from './app.po';

describe('find-github-users App', () => {
  let page: FindGithubUsersPage;

  beforeEach(() => {
    page = new FindGithubUsersPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
